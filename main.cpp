//loop through chunks
//do CRC calculation check

#include <fstream>
#include <iostream>

void output_chars(char* buf, int num, bool as_ints = false) {
  for (int i = 0; i < num; i++) {
    as_ints ? std::cout << int(uint8_t(buf[i])) << " " : std::cout << buf[i] << " ";
  }
  std::cout << std::endl;
}

int main() {
  std::ifstream img;
  img.open("./google.png", std::ios::binary);
  //check if png
  char* buf = new char[1024];
  img.read(buf, 8);
  if (uint8_t(buf[0]) == 137 && buf[1] == 80 && buf[2] == 78 && 
    buf[3] == 71 && buf[4] == 13 && buf[5] == 10 && 
    buf[6] == 26 && buf[7] == 10) {
    std::cout << "valid PNG header" << std::endl;
  }
  memset(buf, 0, sizeof(*buf));
  std::cout << "chunk len: " << std::endl;
  img.read(buf, 4);
  output_chars(buf, 4, true);
  int chunk_len = 0;
  char* cl = (char*)&chunk_len;
  cl[0] = buf[3];
  cl[1] = buf[2];
  cl[2] = buf[1];
  cl[3] = buf[0];

  memset(buf, 0, sizeof(&buf));
  std::cout << "chunk type: " << std::endl;
  img.read(buf, 4);
  output_chars(buf, 4, false);

  memset(buf, 0, sizeof(&buf));
  std::cout << "chunk data: " << std::endl;
  img.read(buf, chunk_len);
  output_chars(buf, chunk_len, true);
  
  memset(buf, 0, sizeof(&buf));
  std::cout << "CRC: " << std::endl;
  img.read(buf, 4);
  output_chars(buf, 4, true);
  return 0;
}
